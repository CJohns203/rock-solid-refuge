# Rock Solid Refuge

Rock Solid Refuge (RSR) was built using WordPress, serving as their primary website and online donation location.

Together our team scoped out the clients needs during an initial discovery phase, which was used to develop and implement an information architecture plan.

Once our designer and project manager had design approval from the client, my role was to build the site with WordPress using Beaver Builder, as well as ensuring all technical requirements were met.

Beaver Builder is a highly effective tool for rapidly building reusable custom responsive layouts in WordPress.


[Rock Solid Refuge site](https://rocksolidrefuge.com)

[Beaver Builder Page Builder](https://www.wpbeaverbuilder.com)

